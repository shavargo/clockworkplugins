--[[
	� 2012 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
	ITEM.name = "Bottled Mountain Spring Water";
	ITEM.cost = 150;
	ITEM.model = "models/props/cs_office/water_bottle.mdl";
	ITEM.weight = 0.35;
	ITEM.access = "1";
	ITEM.uniqueID = "cw_water_mountain";
	ITEM.business = true;
	ITEM.useText = "Drink";
	ITEM.useSound = "npc/barnacle/barnacle_gulp1.wav";
	ITEM.category = "Consumables";
	ITEM.description = "A bottle of clear water, it restores Stamina.";
	
-- Called when a player uses the item.
function ITEM:OnUse(player, itemEntity)
	player:SetCharacterData("stamina", 100);

	player:SetHealth( math.Clamp(player:Health() + 10, 0, 100) );
	
	player:BoostAttribute(self.name, ATB_AGILITY, 6, 600);
	player:BoostAttribute(self.name, ATB_STAMINA, 6, 600);

	local sound = "hgn/stalker/player/pl_softdrink.mp3";
	player:EmitSound(sound); 
end;

	-- Called when a player drops the item.
	function ITEM:OnDrop(player, position) end;
Clockwork.item:Register(ITEM);