--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Canned Tuna";
ITEM.batch = 1;
ITEM.model = "models/stalker/item/food/tuna.mdl";
ITEM.weight = 0.6;
ITEM.cost = 30;
ITEM.business = true;
ITEM.access = "1";
ITEM.useText = "Eat";
ITEM.category = "Consumables";
ITEM.description = "A can of sealed tuna.";

-- Called when a player uses the item.
function ITEM:OnUse(player, itemEntity)
	player:SetHealth( math.Clamp( player:Health() + 5, 0, player:GetMaxHealth() ) );
	local sound = "hgn/stalker/player/pl_food.mp3";
	player:EmitSound(sound); 
end;

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();