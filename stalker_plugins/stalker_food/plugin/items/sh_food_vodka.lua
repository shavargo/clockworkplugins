--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("alcohol_base");
ITEM.name = "Vodka";
ITEM.cost = 30;
ITEM.model = "models/stalker/item/food/vokda.mdl";
ITEM.weight = 0.6;
ITEM.access = "1";
ITEM.business = true;
ITEM.attributes = {Strength = 15};
ITEM.description = "A glass bottle filled with liquid, it has a funny smell.";

-- Called when a player drinks the item.
function ITEM:OnDrink(player)
	local sound = "hgn/stalker/player/pl_vodka.mp3";
	player:EmitSound(sound); 
end;

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();