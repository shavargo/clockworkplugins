local PLUGIN = PLUGIN;
local Clockwork = Clockwork;

--[[

********************
** TnB Animations **
********************

ragdoll
reference
idle_all_01
idle_all_02
idle_all_angry
idle_all_scared
idle_all_cower
cidle_all
swim_idle_all
sit
menu_walk
menu_combine
menu_gman
walk_all
cwalk_all
run_all_01
run_all_02
run_all_panicked_01
run_all_panicked_02
run_all_protected
run_all_charging
swimming_all
walk_suitcase
aimlayer_magic_delta_ref
aimlayer_magic_delta
soldier_Aim_9_directions
weapons_Aim_9_directions
weapons_Aim_9_directions_Alert
aimlayer_ar2
aimlayer_camera
aimlayer_camera_crouch
aimlayer_crossbow
aimlayer_dual
aimlayer_dual_crouch
aimlayer_dual_walk
aimlayer_dual_run
aimlayer_knife
aimlayer_knife_crouch
aimlayer_knife_walk
aimlayer_magic
aimlayer_magic_crouch
aimlayer_melee
aimlayer_melee2
aimlayer_melee2_crouch
aimlayer_physgun
aimlayer_physgun_crouch
aimlayer_physgun_run
aimlayer_pistol
aimlayer_pistol_crouch
aimlayer_pistol_walk
aimlayer_pistol_run
aimlayer_rpg
aimlayer_rpg_crouch
aimlayer_revolver
aimlayer_revolver_walk
aimlayer_revolver_run
aimlayer_slam
aimlayer_slam_crouch
aimlayer_shotgun
aimlayer_smg
cidle_ar2
cidle_crossbow
cidle_camera
cidle_dual
cidle_fist
cidle_grenade
cidle_knife
cidle_magic
cidle_melee
cidle_melee2
cidle_passive
cidle_physgun
cidle_pistol
cidle_revolver
cidle_rpg
cidle_shotgun
cidle_slam
cidle_smg1
cwalk_ar2
cwalk_camera
cwalk_crossbow
cwalk_dual
cwalk_fist
cwalk_knife
cwalk_magic
cwalk_melee2
cwalk_passive
cwalk_pistol
cwalk_physgun
cwalk_revolver
cwalk_rpg
cwalk_shotgun
cwalk_smg1
cwalk_grenade
cwalk_melee
cwalk_slam
idle_ar2
idle_camera
idle_crossbow
idle_dual
idle_knife
idle_grenade
idle_magic
idle_melee
idle_melee2
idle_passive
idle_pistol
idle_physgun
idle_revolver
idle_rpg
idle_shotgun
idle_slam
idle_smg1
jump_ar2
jump_camera
jump_crossbow
jump_dual
jump_fist
jump_grenade
jump_knife
jump_magic
jump_melee
jump_melee2
jump_passive
jump_pistol
jump_physgun
jump_revolver
jump_rpg
jump_shotgun
jump_slam
jump_smg1
run_ar2
run_camera
run_crossbow
run_dual
run_fist
run_knife
run_magic
run_melee2
run_passive
run_physgun
run_revolver
run_rpg
run_shotgun
run_smg1
run_grenade
run_melee
run_pistol
run_slam
sit_ar2
sit_camera
sit_crossbow
sit_duel
sit_fist
sit_grenade
sit_knife
sit_melee
sit_melee2
sit_pistol
sit_shotgun
sit_smg1
sit_physgun
sit_rpg
sit_passive
sit_slam
swim_idle_ar2
swim_idle_camera
swim_idle_crossbow
swim_idle_duel
swim_idle_fist
swim_idle_gravgun
swim_idle_grenade
swim_idle_knife
swim_idle_magic
swim_idle_melee
swim_idle_melee2
swim_idle_passive
swim_idle_pistol
swim_idle_revolver
swim_idle_rpg
swim_idle_shotgun
swim_idle_slam
swim_idle_smg1
swimming_ar2
swimming_camera
swimming_crossbow
swimming_duel
swimming_fist
swimming_gravgun
swimming_magic
swimming_melee2
swimming_passive
swimming_revolver
swimming_rpg
swimming_shotgun
swimming_smg1
swimming_grenade
swimming_knife
swimming_melee
swimming_pistol
swimming_slam
walk_ar2
walk_camera
walk_crossbow
walk_dual
walk_fist
walk_knife
walk_magic
walk_melee2
walk_passive
walk_physgun
walk_revolver
walk_rpg
walk_shotgun
walk_smg1
walk_grenade
walk_melee
walk_pistol
walk_slam
death_01
death_02
death_03
death_04
idle_melee_angry
idle_suitcase
sit_zen
pose_standing_01
pose_standing_02
pose_standing_03
pose_standing_04
pose_ducking_01
pose_ducking_02
seq_cower
seq_preskewer
seq_baton_swing
seq_meleeattack01
seq_throw
seq_wave_smg1
idle_layer
idle_layer_alt
idle_layer_alt_nofeetlock
idle_layer_lock_right
gmod_breath_layer
gmod_breath_layer_lock_right
gmod_breath_layer_lock_hands
gmod_breath_layer_sitting
gmod_breath_noclip_layer
gmod_jump_delta
jump_land
gesture_voicechat
gesture_agree_original
gesture_agree_pelvis_layer
gesture_agree_base_layer
gesture_agree
gesture_bow_original
gesture_bow_pelvis_layer
gesture_bow_base_layer
gesture_bow
gesture_becon_original
gesture_becon_pelvis_layer
gesture_becon_base_layer
gesture_becon
gesture_disagree_original
gesture_disagree_pelvis_layer
gesture_disagree_base_layer
gesture_disagree
gesture_salute_original
gesture_salute_pelvis_layer
gesture_salute_base_layer
gesture_salute
gesture_wave_original
gesture_wave_pelvis_layer
gesture_wave_base_layer
gesture_wave
gesture_item_drop_original
gesture_item_drop_pelvis_layer
gesture_item_drop_base_layer
gesture_item_drop
gesture_item_give_original
gesture_item_give_pelvis_layer
gesture_item_give_base_layer
gesture_item_give
gesture_item_place_original
gesture_item_place_pelvis_layer
gesture_item_place_base_layer
gesture_item_place
gesture_item_throw_original
gesture_item_throw_pelvis_layer
gesture_item_throw_base_layer
gesture_item_throw
gesture_signal_forward_original
gesture_signal_forward_pelvis_layer
gesture_signal_forward_base_layer
gesture_signal_forward
gesture_signal_halt_original
gesture_signal_halt_pelvis_layer
gesture_signal_halt_base_layer
gesture_signal_halt
gesture_signal_group_original
gesture_signal_group_pelvis_layer
gesture_signal_group_base_layer
gesture_signal_group
taunt_cheer_base
taunt_cheer
taunt_dance_base
taunt_dance
taunt_laugh_base
taunt_laugh
taunt_muscle_base
taunt_muscle
taunt_robot_base
taunt_robot
taunt_persistence_base
taunt_persistence
shotgun_pump
fist_block
range_ar2
range_crossbow
range_dual_r
range_dual_l
range_gravgun
range_grenade
range_knife
range_melee
range_melee2_b
range_pistol
range_revolver
range_rpg
range_shotgun
range_slam
range_smg1
reload_ar2_original
reload_ar2_pelvis_layer
reload_ar2_base_layer
reload_ar2
reload_pistol_original
reload_pistol_pelvis_layer
reload_pistol_base_layer
reload_pistol
reload_smg1_original
reload_smg1_pelvis_layer
reload_smg1_base_layer
reload_smg1
reload_smg1_alt_original
reload_smg1_alt_pelvis_layer
reload_smg1_alt_base_layer
reload_smg1_alt
reload_dual_original
reload_dual_pelvis_layer
reload_dual_base_layer
reload_dual
reload_revolver_original
reload_revolver_pelvis_layer
reload_revolver_base_layer
reload_revolver
reload_shotgun_original
reload_shotgun_pelvis_layer
reload_shotgun_base_layer
reload_shotgun
range_melee_shove_2hand_original
range_melee_shove_2hand_pelvis_layer
range_melee_shove_2hand_base_layer
range_melee_shove_2hand
range_melee_shove_1hand_original
range_melee_shove_1hand_pelvis_layer
range_melee_shove_1hand_base_layer
range_melee_shove_1hand
range_melee_shove
flinch_01
flinch_02
flinch_back_01
flinch_head_01
flinch_head_02
flinch_phys_01
flinch_phys_02
flinch_shoulder_l
flinch_shoulder_r
flinch_stomach_01
flinch_stomach_02
idle_fist
idle_fist_layer
idle_fist_layer_weak
idle_fist_layer_rt
range_fists_r
range_fists_l
drive_pd
sit_rollercoaster
drive_airboat
drive_jeep
zombie_anim_fix_pelvis_layer
zombie_anim_fix_base_layer
zombie_anim_fix
zombie_crouch_layer_pelvis
zombie_idle_01
zombie_cidle_01
zombie_cidle_02
zombie_walk_01
zombie_walk_02
zombie_walk_03
zombie_walk_04
zombie_walk_05
zombie_cwalk_01
zombie_cwalk_02
zombie_cwalk_03
zombie_cwalk_04
zombie_cwalk_05
zombie_walk_06
zombie_run_upperbody_layer
zombie_run
zombie_run_fast
zombie_attack_01_original
zombie_attack_01_pelvis_layer
zombie_attack_01_base_layer
zombie_attack_01
zombie_attack_02_original
zombie_attack_02_pelvis_layer
zombie_attack_02_base_layer
zombie_attack_02
zombie_attack_03_original
zombie_attack_03_pelvis_layer
zombie_attack_03_base_layer
zombie_attack_03
zombie_attack_04_original
zombie_attack_04_pelvis_layer
zombie_attack_04_base_layer
zombie_attack_04
zombie_attack_05_original
zombie_attack_05_pelvis_layer
zombie_attack_05_base_layer
zombie_attack_05
zombie_attack_06_original
zombie_attack_06_pelvis_layer
zombie_attack_06_base_layer
zombie_attack_06
zombie_attack_07_original
zombie_attack_07_pelvis_layer
zombie_attack_07_base_layer
zombie_attack_07
zombie_attack_special_original
zombie_attack_special_pelvis_layer
zombie_attack_special_base_layer
zombie_attack_special
zombie_attack_frenzy_original
zombie_attack_frenzy_pelvis_layer
zombie_attack_frenzy_base_layer
zombie_attack_frenzy
taunt_zombie_original
taunt_zombie_pelvis_layer
taunt_zombie_base_layer
taunt_zombie
menu_zombie_01
zombie_climb_start
zombie_climb_loop
zombie_climb_end
zombie_leap_start
zombie_leap_mid
zombie_slump_idle_02
zombie_slump_rise_02_fast
zombie_slump_rise_02_slow
zombie_slump_idle_01
zombie_slump_rise_01
body_rot_z
head_rot_z
head_rot_y

--]]

-- Clockwork.animation.stored.tnbHuman = {
-- 	["crouch_grenade_aim_idle"] = "cidle_all",
-- 	["crouch_grenade_aim_walk"] = "cwalk_all",
-- 	["stand_grenade_aim_idle"] = "idle_all_01",
-- 	["crouch_pistol_aim_idle"] = "aimlayer_pistol_crouch",
-- 	["stand_grenade_aim_walk"] = "walk_all",
-- 	["crouch_pistol_aim_walk"] = "aimlayer_ar2",
-- 	["crouch_heavy_aim_idle"] = "aimlayer_smg",
-- 	["crouch_blunt_aim_idle"] = "aimlayer_melee2",
-- 	["stand_grenade_aim_run"] = "run_all_01",
-- 	["crouch_blunt_aim_walk"] = "cwalk_ar2",
-- 	["crouch_heavy_aim_walk"] = "aimlayer_ar2",
-- 	["stand_pistol_aim_walk"] = "aimlayer_pistol_walk",
-- 	["stand_pistol_aim_idle"] = "aimlayer_pistol",
-- 	["crouch_fist_aim_walk"] = "aimlayer_ar2",
-- 	["crouch_slam_aim_walk"] = "aimlayer_slam_crouch",
-- 	["stand_pistol_aim_run"] = "aimlayer_ar2",
-- 	["crouch_fist_aim_idle"] = "aimlayer_smg",
-- 	["stand_heavy_aim_idle"] = "aimlayer_ar2",
-- 	["stand_blunt_aim_idle"] = "aimlayer_melee",
-- 	["crouch_slam_aim_idle"] = "crouch_slam_idle",
-- 	["stand_blunt_aim_walk"] = "aimlayer_ar2",
-- 	["stand_heavy_aim_walk"] = "aimlayer_ar2",
-- 	["stand_fist_aim_idle"] = "idle_fist",
-- 	["crouch_smg_aim_walk"] = "aimlayer_ar2",
-- 	["crouch_smg_aim_idle"] = "aimlayer_smg",
-- 	["stand_fist_aim_walk"] = "walk_fist",
-- 	["stand_blunt_aim_run"] = "aimlayer_melee2",
-- 	["stand_heavy_aim_run"] = "aimlayer_rpg",
-- 	["crouch_grenade_walk"] = "cwalk_all",
-- 	["crouch_grenade_idle"] = "cidle_all",
-- 	["stand_slam_aim_idle"] = "aimlayer_slam",
-- 	["stand_slam_aim_walk"] = "walk_slam",
-- 	["stand_slam_aim_run"] = "run_rpg",
-- 	["stand_smg_aim_idle"] = "aimlayer_smg",
-- 	["stand_smg_aim_walk"] = "aimlayer_smg",
-- 	["stand_fist_aim_run"] = "run_fist",
-- 	["crouch_pistol_idle"] = "cidle_all",
-- 	["stand_grenade_walk"] = "walk_all",
-- 	["crouch_pistol_walk"] = "cwalk_all",
-- 	["stand_grenade_idle"] = "idle_all_01",
-- 	["stand_grenade_run"] = "run_all_01",
-- 	["crouch_blunt_idle"] = "cidle_all",
-- 	["stand_pistol_walk"] = "walk_all",
-- 	["crouch_blunt_walk"] = "cwalk_all",
-- 	["crouch_heavy_walk"] = "cwalk_ar2",
-- 	["stand_pistol_idle"] = "idle_all_01",
-- 	["crouch_heavy_idle"] = "idle_rpg",
-- 	["stand_smg_aim_run"] = "aimlayer_ar2",
-- 	["stand_heavy_walk"] = "walk_rpg",
-- 	["stand_blunt_walk"] = "walk_all",
-- 	["stand_blunt_idle"] = "idle_all_01",
-- 	["crouch_fist_idle"] = "cidle_all",
-- 	["crouch_fist_walk"] = "cwalk_all",
-- 	["crouch_slam_idle"] = "crouch_idle",
-- 	["stand_pistol_run"] = "run_all_01",
-- 	["stand_heavy_idle"] = "idle_rpg",
-- 	["crouch_slam_walk"] = "cwalk_slam",
-- 	["stand_heavy_run"] = "run_rpg",
-- 	["stand_slam_idle"] = "idle_all_02",
-- 	["stand_fist_walk"] = "walk_fist",
-- 	["stand_slam_walk"] = "walk_slam",
-- 	["stand_blunt_run"] = "run_all_01",
-- 	["crouch_smg_walk"] = "cwalk_rpg",
-- 	["crouch_smg_idle"] = "idle_rpg",
-- 	["stand_fist_idle"] = "idle_all_01",
-- 	["stand_slam_run"] = "run_all_01",
-- 	["grenade_attack"] = "gesture_item_throw",
-- 	["stand_smg_idle"] = "idle_smg1",
-- 	["stand_fist_run"] = "run_all_01",
-- 	["stand_smg_walk"] = "walk_smg1",
-- 	["pistol_attack"] = "aimlayer_pistol",
-- 	["stand_smg_run"] = "run_smg1",
-- 	["pistol_reload"] = "reload_pistol",
-- 	["heavy_reload"] = "reload_ar2",
-- 	["heavy_attack"] = "aimlayer_ar2",
-- 	["blunt_attack"] = "aimlayer_melee",
-- 	["crouch_idle"] = "cidle_all",
-- 	["crouch_walk"] = "cwalk_all",
-- 	["slam_attack"] = "aimlayer_slam",
-- 	["stand_idle"] = "idle_all_01",
-- 	["stand_walk"] = "walk_all",
-- 	["smg_attack"] = "aimlayer_smg1",
-- 	["smg_reload"] = "reload_smg1",
-- 	["stand_run"] = "run_all_01",
-- 	["jump"] = "swimming_fist",
-- 	["sit"] = "sit_passive"
-- };

Clockwork.animation.stored.tnbHuman = {
	["crouch_grenade_aim_idle"] = "cidle_grenade",
	["crouch_grenade_aim_walk"] = "cwalk_grenade",
	["stand_grenade_aim_idle"] = "idle_grenade",
	["crouch_pistol_aim_idle"] = "cidle_revolver",
	["stand_grenade_aim_walk"] = "walk_grenade",
	["crouch_pistol_aim_walk"] = "cwalk_revolver",
	["crouch_heavy_aim_idle"] = "cidle_physgun",
	["crouch_blunt_aim_idle"] = "cidle_melee",
	["stand_grenade_aim_run"] = "run_grenade",
	["crouch_blunt_aim_walk"] = "cwalk_melee",
	["crouch_heavy_aim_walk"] = "cwalk_physgun",
	["stand_pistol_aim_walk"] = "walk_revolver",
	["stand_pistol_aim_idle"] = "idle_revolver",
	["crouch_fist_aim_walk"] = "cwalk_fist",
	["crouch_slam_aim_walk"] = "cwalk_slam",
	["stand_pistol_aim_run"] = "run_revolver",
	["crouch_fist_aim_idle"] = "cidle_fist",
	["stand_heavy_aim_idle"] = "idle_physgun",
	["stand_blunt_aim_idle"] = "idle_melee",
	["crouch_slam_aim_idle"] = "cidle_slam",
	["stand_blunt_aim_walk"] = "walk_melee",
	["stand_heavy_aim_walk"] = "walk_physgun",
	["stand_fist_aim_idle"] = "idle_fist",
	["crouch_smg_aim_walk"] = "cwalk_smg1",
	["crouch_smg_aim_idle"] = "cidle_smg1",
	["stand_fist_aim_walk"] = "walk_fist",
	["stand_blunt_aim_run"] = "run_melee",
	["stand_heavy_aim_run"] = "run_physgun",
	["crouch_grenade_walk"] = "cwalk_all",
	["crouch_grenade_idle"] = "cidle_all",
	["stand_slam_aim_idle"] = "idle_slam",
	["stand_slam_aim_walk"] = "walk_slam",
	["stand_slam_aim_run"] = "run_slam",
	["stand_smg_aim_idle"] = "idle_smg1",
	["stand_smg_aim_walk"] = "walk_smg1",
	["stand_fist_aim_run"] = "run_passive",
	["crouch_pistol_idle"] = "cidle_all",
	["stand_grenade_walk"] = "walk_passive",
	["crouch_pistol_walk"] = ACT_MP_CROUCHWALK,
	["stand_grenade_idle"] = "idle_all_01",
	["stand_grenade_run"] = "run_passive",
	["crouch_blunt_idle"] = "cidle_all",
	["stand_pistol_walk"] = "walk_passive",
	["crouch_blunt_walk"] = "cwalk_all",
	["crouch_heavy_walk"] = "cwalk_passive",
	["stand_pistol_idle"] = "idle_all_01",
	["crouch_heavy_idle"] = "cidle_passive",
	["stand_smg_aim_run"] = "run_smg1",
	["stand_heavy_walk"] = "walk_passive",
	["stand_blunt_walk"] = "walk_passive",
	["stand_blunt_idle"] = "idle_all_01",
	["crouch_fist_idle"] = "cidle_all",
	["crouch_fist_walk"] = "cwalk_all",
	["crouch_slam_idle"] = "cidle_all",
	["stand_pistol_run"] = "run_passive",
	["stand_heavy_idle"] = "idle_passive",
	["crouch_slam_walk"] = "cwalk_all",
	["stand_heavy_run"] = "run_passive",
	["stand_slam_idle"] = "cidle_all",
	["stand_fist_walk"] = "walk_all",
	["stand_slam_walk"] = "walk_passive",
	["stand_blunt_run"] = "run_passive",
	["crouch_smg_walk"] = "cwalk_passive",
	["crouch_smg_idle"] = "cidle_passive",
	["stand_fist_idle"] = "idle_all_01",
	["stand_slam_run"] = "run_passive",
	["grenade_attack"] = ACT_RANGE_ATTACK_THROW,
	["stand_smg_idle"] = "idle_passive",
	["stand_fist_run"] = "run_all_01",
	["stand_smg_walk"] = "walk_passive",
	["pistol_attack"] = ACT_GESTURE_RANGE_ATTACK_PISTOL,
	["stand_smg_run"] = "run_passive",
	["pistol_reload"] = ACT_GESTURE_RELOAD_PISTOL,
	["heavy_reload"] = ACT_GESTURE_RELOAD_SMG1,
	["heavy_attack"] = ACT_GESTURE_RANGE_ATTACK_AR2,
	["blunt_attack"] = ACT_MELEE_ATTACK_SWING,
	["crouch_idle"] = "cidle_all",
	["crouch_walk"] = "cwalk_all",
	["slam_attack"] = ACT_PICKUP_GROUND,
	["stand_idle"] = "idle_all_01",
	["stand_walk"] = "walk_all",
	["smg_attack"] = ACT_GESTURE_RANGE_ATTACK_SMG1,
	["smg_reload"] = ACT_GESTURE_RELOAD_SMG1,
	["stand_run"] = "run_all",
	["jump"] = "jump_passive",
	["sit"] = ACT_BUSY_SIT_CHAIR
};

-- A function to add a TnB human model.
function Clockwork.animation:AddTNBModel(model)
	return self:AddModel("tnbHuman", model);
end;

Clockwork.animation:AddTNBModel("models/tnb/stalker/female_01_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_01_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_02_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_02_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_03_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_03_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_04_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_04_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_05_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_05_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_06_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_06_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_07_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_07_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_08_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_08_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_sunrise_balaclava.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_sunrise_combat.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_sunrise_gasmask.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/female_sunrise_helmet.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_01_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_01_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_02_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_02_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_03_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_03_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_04_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_04_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_05_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_05_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_06_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_06_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_07_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_07_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_08_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_08_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_09_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_09_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_10_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_10_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_anorak.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_bandit.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_bandit_gasmask.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_berill.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_berill_helmet.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_berill_seva.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_cs1.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_cs2.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_cs3.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_cs3_mask.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_exo.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_exo_bandit.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_io7a.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_io7a_bandit.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_io8a.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_radsuit.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_radsuit_seva.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_radsuit_tubes.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_seva.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_skat.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_skat_bulat.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_ssp.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_sunrise.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_sunrise_balaclava.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_sunrise_combat.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_sunrise_gasmask.mdl");
Clockwork.animation:AddTNBModel("models/tnb/stalker/male_sunrise_helmet.mdl");
