local PLUGIN = PLUGIN;

Clockwork.kernel:IncludePrefixed("sv_hooks.lua");

Clockwork.option:SetKey("menu_music", "");
Clockwork.option:SetKey("name_cash", "Rubles");
Clockwork.option:SetKey("model_cash", "models/props/cs_assault/Money.mdl");


Clockwork.flag:Add("1", "Tier 1 Trader", "Access to tier one goods.");
Clockwork.flag:Add("2", "Tier 2 Trader", "Access to tier two goods.");
Clockwork.flag:Add("3", "Tier 3 Trader", "Access to tier three goods.");
Clockwork.flag:Add("4", "Tier 4 Trader", "Access to tier four goods.");
Clockwork.flag:Add("5", "Tier 5 Trader", "Access to tier five goods.");

Clockwork.flag:Add("9", "Technician Weapon Upgrades", "Access to upgrading weapons.");
Clockwork.flag:Add("0", "Technician Armors", "Access to all armors.");