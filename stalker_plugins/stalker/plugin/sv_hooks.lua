-- Called when CWU character is made.
function PLUGIN:GetPlayerDefaultInventory(player, character, inventory)
	if (character.faction == FACTION_CITIZEN) then
		return nil;
	elseif (character.faction == FACTION_DUTY) then
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("handheld_radio"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("pda"));
	elseif (character.faction == FACTION_FREEDOM) then
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("handheld_radio"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("pda"));
	elseif (character.faction == FACTION_UKM) then
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("handheld_radio"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("pda"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("stalker_ammo_rifle"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("stalker_ammo_rifle"));
		Clockwork.inventory:AddInstance(inventory, Clockwork.item:CreateInstance("stalker_ak74"));
	end;
end;

function PLUGIN:PlayerPlayDeathSound(player, gender)
	local sound = "hgn/stalker/player/death1.mp3";

	-- for k, v in ipairs( _player.GetAll() ) do
	-- 	if (v:HasInitialized()) then
	-- 		if (self:PlayerIsCombine(v)) then
	-- 			v:EmitSound(sound);
	-- 		end;
	-- 	end;
	-- end;

	return sound;
end;

function PLUGIN:OnNPCKilled(npc, attacker, inflictor)
	local weapon = npc:GetActiveWeapon()
	if (IsValid(weapon)) then
		weapon:Remove();
	end;
end;

function PLUGIN:PlayerGiveWeapons(player)
	Clockwork.player:GiveSpawnWeapon(player, "stalker_bolt");
end;

-- Called when a player's pain sound should be played.
-- function PLUGIN:PlayerPlayPainSound(player, gender, damageInfo, hitGroup)
-- 	return
-- end;

-- local name = player:Name();
-- local faction = player:GetFaction();