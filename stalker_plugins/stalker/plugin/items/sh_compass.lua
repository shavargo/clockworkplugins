--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Compass";
ITEM.cost = 40000;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/compass.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "A rare artifact � it's been several years since one was found. The artifact can locate gaps in anomaly fields, effectively acting as a compass. It is believed that it can help one traverse the most complex anomaly fields completely unharmed. However, very few know how to handle it properly.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();