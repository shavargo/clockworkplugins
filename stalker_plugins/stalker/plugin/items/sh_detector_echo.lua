--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Echo Detector";
ITEM.cost = 1800;
ITEM.model = "models/kali/miscstuff/stalker/detector_echo.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Detector";
ITEM.description = "This dated anomalous activity detector produces an alarm signal when anomalous particles enter a special chamber as you approach an anomaly. It can also register the presence of artifacts and measure the distance to the nearest one. The distance is indicated by the frequency of the sound the detector emits. To switch to artifact search mode, open the device's front cover. Due to its simple design, Echo can only be used to locate the most common artifacts.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();