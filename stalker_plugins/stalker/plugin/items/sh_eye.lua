--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Eye";
ITEM.cost = 3500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/eye.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This artifact, which resembles the human eye, considerably increases the body's metabolism, helping wounds heal quicker. Experienced stalkers say that the Eye also brings luck. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();