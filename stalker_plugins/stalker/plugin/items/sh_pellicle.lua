--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Pellicle";
ITEM.cost = 30000;
ITEM.model = "models/srp/items/art_bubble.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This artifact is so rare that many researchers can't even imagine that such a substance can exist in a natural setting. Emits acidic chemical components.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();