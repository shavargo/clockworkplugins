--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Spring";
ITEM.cost = 3500;
ITEM.model = "models/srp/items/art_spring.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "According to the opinion of some researchers-theorists, this artifact is a hybrid between Batteries and Shell.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();