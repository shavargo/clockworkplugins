--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Wrenched";
ITEM.cost = 4250;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/wrenched.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This bizarrely-shaped artifact appears in places with increased gravitational activity. Acting as a kind of sponge that absorbs radioactive elements, this artifact provides protection from outside radiation as well as from radioactive particles that have already made their way into the body.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();