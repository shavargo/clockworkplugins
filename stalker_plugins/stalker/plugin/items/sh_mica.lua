--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Mica";
ITEM.cost = 25000;
ITEM.model = "models/srp/items/art_mica.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The Fruit Punch anomaly is able to create such an artifact at the rarest, most extreme collection of physical conditions. The result is a semi-transparent, hard object. A rare and expensive artifact.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();