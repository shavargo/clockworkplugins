--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Oasis";
ITEM.cost = 50000;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/kolobok.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "A crystalized plant that took over incredible healing properties through some anomaly of science. The artifact is extremely beneficial to its target-- and extremely radioactive.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();