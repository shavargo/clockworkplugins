--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Dementor";
ITEM.cost = 15000;
ITEM.model = "models/srp/items/spezzy/art_vrchen.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The tales of the Dementor were originally a warning given to greedy stalkers that came to the Zone for material profit through artifact hunting. Used to dissuade people from losing sight of their humanity through their greed, the tales discribed an artifact that destroyed the user's mind. They were only rumors ... right?";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();