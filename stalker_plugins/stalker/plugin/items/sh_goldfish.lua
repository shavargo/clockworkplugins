--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("backpack_base");
ITEM.name = "Goldfish";
ITEM.cost = 8000;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/goldfish.mdl";
ITEM.weight = 0.5;
ITEM.actualWeight = 0.5;
ITEM.invSpace = 12;
ITEM.slot = "artifact";
ITEM.slotSpace = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The result of an interaction between a large number of gravitational fields, Goldfish produces its own powerful internally directed gravitational field, which renders items within its range almost weightless. This artifact is most effective for reducing carried weight, placing it always in high demand. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();