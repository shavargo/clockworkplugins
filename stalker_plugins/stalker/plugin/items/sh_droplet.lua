--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Droplet";
ITEM.cost = 750;
ITEM.model = "models/srp/items/art_droplet.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Formed in the Burner anomaly at high temperatures. From the exterior, looks like a tear-like shade compound with a glossy surface, covered in cracks.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();