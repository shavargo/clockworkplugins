--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Veles Detector";
ITEM.cost = 6400;
ITEM.model = "models/kali/miscstuff/stalker/detector_veles.mdl";
ITEM.weight = 0.5;
ITEM.access = "3";
ITEM.business = false;
ITEM.category = "Detector";
ITEM.description = "A next generation scientific detection scanner. Thanks to three modernized detection chambers, the position of artifacts is shown on a special display screen. When closed, the device registers only radiation and anomalies. To switch to artifact search mode, open the front LED display. When in search mode, the detector is capable of locating all artifacts known to science.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();