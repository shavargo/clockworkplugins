--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Crystal Thorn";
ITEM.cost = 2500;
ITEM.model = "models/srp/items/art_crystalthorn.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Crystallizes in the anomaly Burnt Fuzz . Naturally takes out the radiation from the organism. That is, through the ears along with some amount of blood. Blood loss is possible also through other openings. Widespread and quite effective.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();