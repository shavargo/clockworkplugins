--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("backpack_base");
ITEM.name = "Night Star";
ITEM.cost = 1250;
ITEM.model = "models/srp/items/art_nightstar.mdl";
ITEM.weight = 0.5;
ITEM.actualWeight = 0.5;
ITEM.invSpace = 4;
ITEM.slot = "artifact";
ITEM.slotSpace = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This glowing artifact can generate a local directed low-gravity field. Widely used by stalkers along with Gravi, a similar artifact, to increase maximum load. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();