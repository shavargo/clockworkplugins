--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Fireball";
ITEM.cost = 4500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/fireball.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Even though Fireball is a thermal artifact, it can be handled without fear of being burned. This artifact is valued for its ability to maintain a temperature of 75 degrees Fahrenheit within a small radius, virtually regardless of its surroundings. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();