--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Wormwood";
ITEM.cost = 35000;
ITEM.model = "models/srp/items/art_soul.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Stalkers have only heard of the fabled Wormwood artifact. Allegedly, it is created through the combining of various other artifacts in a natural setting, and can cure any illness-- but nobody has ever seen one to be able to tell if the rumors are true.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();