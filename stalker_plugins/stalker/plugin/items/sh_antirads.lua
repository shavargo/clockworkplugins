--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Anti-Rad";
ITEM.cost = 200;
ITEM.model = "models/stalker/item/medical/antirad.mdl";
ITEM.weight = 0.2;
ITEM.access = "1";
ITEM.useText = "Apply";
ITEM.category = "Medical"
ITEM.business = false;
ITEM.useSound = "items/medshot4.wav";
ITEM.blacklist = {CLASS_MPR};
ITEM.description = "A packet filled with medication for radiation.";
ITEM.customFunctions = {"Give"};

-- Called when a player uses the item.
function ITEM:OnUse(player, itemEntity)
	-- player:SetHealth( math.Clamp( player:Health() + Schema:GetHealAmount(player, 2), 0, player:GetMaxHealth() ) );
	
	-- Clockwork.plugin:Call("PlayerHealed", player, player, self);
end;

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

-- if (SERVER) then
-- 	function ITEM:OnCustomFunction(player, name)
-- 		if (name == "Give") then
-- 			Clockwork.player:RunClockworkCommand(player, "CharHeal", "regular_health_kit");
-- 		end;
-- 	end;
-- end;

ITEM:Register();