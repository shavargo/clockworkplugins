--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Stone Flower";
ITEM.cost = 2500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/stone flower.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This artifact is made up of a granite-like stone deformed by an extreme gravitational force, resulting in a crystallized formation that has not been explained by science to date. Can provide moderate protection against psy-emissions and is radioactive.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();