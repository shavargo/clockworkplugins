--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Slime";
ITEM.cost = 1000;
ITEM.model = "models/srp/items/art_slime.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "It is certain that this artifact is created by the anomaly called 'Fruit Punch'. When carried on the belt, the wounds bleed less, although the body of its owner becomes vulnerable to various burns.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();