--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Bubble";
ITEM.cost = 3000;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/bubble.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "A compound of several hollow organic formations, this artifact emits a gaseous substance that can neutralize radioactive particles inside the body without harming it. Because of its effectiveness, this artifact is in great demand.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();