--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Battery";
ITEM.cost = 1500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/battery.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The composition of this artifact includes electrostatic elements, but scientists have yet to identify the exact physical conditions required for its formation. Popular in the Zone and valued by its residents and visitors for its energizing properties, though it can tire the body out through extended use. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();