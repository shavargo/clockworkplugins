--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Joker";
ITEM.cost = 40000;
ITEM.model = "models/srp/items/spezzy/art_moldfish.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Rumors in the early days of the Zone spoke of a stalker that would miraculously survive shootouts with his enemies-- only to fall injured days later, alone. The story eventually faded away, with the stalker being dismissed as a simple myth meant to deter people from traveling alone.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();