--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("backpack_base");
ITEM.name = "Gravi";
ITEM.cost = 2500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/gravi.mdl";
ITEM.weight = 0.5;
ITEM.actualWeight = 0.5;
ITEM.invSpace = 8;
ITEM.slot = "artifact";
ITEM.slotSpace = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Gravi is formed from metallic substances exposed to prolonged gravitation. This makes it capable of sustaining an antigravitational field, and many stalkers use it to reduce the weight of their backpacks. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();