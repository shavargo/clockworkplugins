--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Slug";
ITEM.cost = 6000;
ITEM.model = "models/srp/items/art_slug.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "Formed by the 'Fruit Punch' anomaly. The negative qualities of this artifact are compensated by the fact that it heightens the coagulation quality of blood. It's not often that one runs into such an artifact, and they pay well for it too.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();