--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Urchin";
ITEM.cost = 10000;
ITEM.model = "models/srp/items/art_urchen.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The anomaly Burnt Fuzz very rarely gives rise to this artifact. Blood pressure rises, the body gets rid of a large amount of red blood cells. But along with them the stored radiation leaves the body as well. It's not realistic to create such an artifact in lab conditions in the next ten years.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();