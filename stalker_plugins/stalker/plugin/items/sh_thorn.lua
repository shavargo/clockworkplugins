--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Thorn";
ITEM.cost = 1100;
ITEM.model = "models/srp/items/art_thorn.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "The result of the interaction between the anomaly Burnt Fuzz and the body of a careless stalker. The Thorn artifact pokes the body of its owner, no matter what. But it also helps clean the body of radionucliodes. Quite widespread and cheap.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();