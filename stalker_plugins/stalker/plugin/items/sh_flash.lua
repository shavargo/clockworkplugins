--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Flash";
ITEM.cost = 3500;
ITEM.model = "models/srp/items/art_flash.mdl";
ITEM.weight = 0.5;
ITEM.access = "";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "This electrostatic artifact is a powerful absorbent of electricity, which it later discharges. Flash is capable of protecting its bearer from electric shocks of up to 5,000 volts. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();