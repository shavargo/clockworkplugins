--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Stone Blood";
ITEM.cost = 2500;
ITEM.model = "models/kali/miscstuff/stalker/artifacts/stone blood.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Artifacts";
ITEM.description = "A reddish formation of compressed and fossilized plants, soil and animal debris. Can partially neutralize chemical poisons. Emits radiation.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();