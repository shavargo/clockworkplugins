--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "PDA";
ITEM.cost = 50;
ITEM.model = "models/stalker/item/handhelds/pda.mdl";
ITEM.weight = 0.1;
ITEM.access = "1";
ITEM.category = "Communication";
ITEM.business = false;
ITEM.description = "A medium device for missions and etc.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();