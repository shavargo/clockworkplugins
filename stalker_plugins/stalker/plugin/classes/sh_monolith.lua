--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Monolith");
	CLASS.color = Color(87, 18, 128, 255);
	CLASS.factions = {FACTION_MONOLITH};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A resistane member.";
	CLASS.defaultPhysDesc = "Wearing dirty clothes.";
CLASS_MONOLITH = CLASS:Register();