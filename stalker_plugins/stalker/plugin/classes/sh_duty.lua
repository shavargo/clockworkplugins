local CLASS = Clockwork.class:New("Duty");

CLASS.color = Color(255, 100, 100, 255);
CLASS.factions = {FACTION_DUTY};
CLASS.isDefault = true;
CLASS.wagesName = "Wages";
CLASS.description = "A paramilitary clan of stalkers operating in the Zone with members living according to a code.";
CLASS.defaultPhysDesc = "";
	
CLASS_DUTY = CLASS:Register();