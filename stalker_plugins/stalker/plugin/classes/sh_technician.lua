--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Technician");
	CLASS.color = Color(150, 125, 100, 255);
	CLASS.factions = {FACTION_TECHNICIAN};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A resistane member.";
	CLASS.defaultPhysDesc = "Wearing dirty clothes.";
CLASS_TECHNICIAN = CLASS:Register();