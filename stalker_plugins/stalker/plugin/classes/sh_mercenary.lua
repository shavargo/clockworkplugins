--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Mercenary");
	CLASS.color = Color(150, 125, 100, 255);
	CLASS.factions = {FACTION_MERC};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A Mercenary.";
	CLASS.defaultPhysDesc = "";
CLASS_MERC = CLASS:Register();