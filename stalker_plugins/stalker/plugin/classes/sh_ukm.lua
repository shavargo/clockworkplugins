--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Ukrainian Military");
	CLASS.color = Color(28, 37, 166, 255);
	CLASS.factions = {FACTION_UKM};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A resistane member.";
	CLASS.defaultPhysDesc = "Wearing dirty clothes.";
CLASS_UKM = CLASS:Register();