local CLASS = Clockwork.class:New("Freedom");

CLASS.color = Color(0, 100, 0, 255);
CLASS.factions = {FACTION_FREEDOM};
CLASS.isDefault = true;
CLASS.wagesName = "Wages";
CLASS.description = "Anarchists and daredevils who declare themselves to be fighting for free access to the Zone and consequently find themselves in constant conflict with army units, military stalkers, and Duty.";
CLASS.defaultPhysDesc = "";
	
CLASS_FREEDOM = CLASS:Register();