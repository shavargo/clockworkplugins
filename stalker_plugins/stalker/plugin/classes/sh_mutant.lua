--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Mutant");
	CLASS.color = Color(237, 171, 85, 255);
	CLASS.factions = {FACTION_MUTANT};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A citizen turned wrong by the UU infection.";
	CLASS.defaultPhysDesc = "Wearing dirty clothes.";
CLASS_MUTANT = CLASS:Register();