--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Bandit");
	CLASS.color = Color(150, 125, 100, 255);
	CLASS.factions = {FACTION_BANDIT};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A Bandit.";
	CLASS.defaultPhysDesc = "";
CLASS_BANDIT = CLASS:Register();