--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local CLASS = Clockwork.class:New("Ecologist");
	CLASS.color = Color(26, 138, 28, 255);
	CLASS.factions = {FACTION_ECOLOGIST};
	CLASS.isDefault = true;
	CLASS.wagesName = "Supplies";
	CLASS.description = "A resistane member.";
	CLASS.defaultPhysDesc = "Wearing dirty clothes.";
CLASS_ECOLOGIST = CLASS:Register();