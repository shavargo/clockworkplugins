--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local FACTION = Clockwork.faction:New("Mutant");

FACTION.useFullName = true;
FACTION.whitelist = true;
FACTION.material = "halfliferp/factions/mutant";
FACTION.models = {
	female = {
		"models/stalkertnb/dog1.mdl",
		"models/stalkertnb/dog2.mdl",
		"models/stalkertnb/zombie1.mdl",
		"models/stalkertnb/zombie2.mdl"
	},	
	male = {
		"models/stalkertnb/dog1.mdl",
		"models/stalkertnb/dog2.mdl",
		"models/stalkertnb/zombie1.mdl",
		"models/stalkertnb/zombie2.mdl"
	};
};

FACTION_MUTANT = FACTION:Register();