--[[
	� 2012 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local FACTION = Clockwork.faction:New("Freedom");

FACTION.useFullName = true;
FACTION.whitelist = true;
FACTION.material = "stalkerroleplay/faction_freedom";
FACTION.autoRecognise = true;
FACTION.ranks = {
	Recruit = {
		default = true,
		position = 4
	},
	Seedy = {
		position = 3
	},

	["war-Dog"] = {
		position = 2
	},

	Ace = {
		position = 1
	}
};

FACTION_FREEDOM = FACTION:Register();