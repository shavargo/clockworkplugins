--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local COMMAND = Clockwork.command:New("GPDA");
COMMAND.tip = "Send global PDA messages to others.";
COMMAND.text = "<string Text>";
COMMAND.flags = bit.bor(CMD_DEFAULT, CMD_DEATHCODE);
COMMAND.arguments = 1;

-- Called when the command has been run.
function COMMAND:OnRun(player, arguments)
	local msg = table.concat(arguments, " ")
	local listeners = {}

	local username = player:Name();

	if (player:GetCharacterData("PDAUSER")) then
		username = player:GetCharacterData("PDAUSER");
	end;
	
	if(msg) then
		if(player:HasItemByID("pda")) then
			for k, v in pairs(_player.GetAll()) do
				if(IsValid(v) and v:HasInitialized() and v:HasItemByID("pda")) then
					table.insert(listeners, v)
				end;
			end;
			Clockwork.chatBox:SendColored(listeners, Color(128, 64, 255), "[G-PDA]: ", Color(0, 196, 255), "["..username.."]", " ", Color(255, 255, 255), msg)

		else
			Clockwork.player:Notify(player, "You don't own a PDA!")
		end;
	else
		Clockwork.player:Notify(player, "You didn't specify a message!")
	end;
end;

COMMAND:Register();