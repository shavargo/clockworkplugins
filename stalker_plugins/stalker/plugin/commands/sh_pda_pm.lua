--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local COMMAND = Clockwork.command:New("PDA");
COMMAND.tip = "Send private PDA messages to others.";
COMMAND.text = "<string Text> <string Text>";
COMMAND.flags = bit.bor(CMD_DEFAULT, CMD_DEATHCODE);
COMMAND.arguments = 2;

-- Called when the command has been run.
function COMMAND:OnRun(player, arguments)
	local msg = arguments[2]
	local target = Clockwork.player:FindByID(arguments[1])

	local username = player:Name();

	if (player:GetCharacterData("PDAUSER")) then
		username = player:GetCharacterData("PDAUSER");
	end;

	if (target) then
		if(msg) then
			if(player:HasItemByID("pda")) then
				if (target:HasItemByID("pda")) then
					Clockwork.chatBox:SendColored(target, Color(255, 50, 50), "[PDA]: ", Color(0, 196, 255), "["..username.."]", ": ", Color(255, 255, 255), msg)
					Clockwork.chatBox:SendColored(player, Color(255, 50, 50), "[PDA]: ", Color(0, 196, 255), "["..username.."]", ": ", Color(255, 255, 255), msg)
				else
					Clockwork.player:Notify(player, "The target don't own a PDA!")
				end;
			else
				Clockwork.player:Notify(player, "You don't own a PDA!")
			end;
		else
			Clockwork.player:Notify(player, "You didn't specify a message!")
		end;
	else
		Clockwork.player:Notify(player, "You didn't specify a target!")
	end;
end;

COMMAND:Register();