--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local COMMAND = Clockwork.command:New("SetUserName");
COMMAND.tip = "Set a Username for PDA.";
COMMAND.text = "<string Text>";
COMMAND.flags = bit.bor(CMD_DEFAULT, CMD_DEATHCODE);
COMMAND.arguments = 1;

-- Called when the command has been run.
function COMMAND:OnRun(player, arguments)
	local msg = table.concat(arguments, " ")
	local username = player:GetCharacterData("PDAUSER");
	
	if(msg) then
		if (!username) then
			player:SetCharacterData("PDAUSER", msg);
			Clockwork.player:Notify(player, "Your PDA username is now: "..msg)
		else
			Clockwork.player:Notify(player, "You already set a username! Notify the Staff Team if you want it removed.")
		end;
	else
		Clockwork.player:Notify(player, "You didn't specify a username!")
	end;
end;

COMMAND:Register();