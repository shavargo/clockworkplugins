--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local COMMAND = Clockwork.command:New("UnsetUserName");
COMMAND.tip = "Removes the Username for someone's PDA.";
COMMAND.text = "<Text>";
COMMAND.flags = CMD_DEFAULT;
COMMAND.access = "o";
COMMAND.arguments = 1;

-- Called when the command has been run.
function COMMAND:OnRun(player, arguments)
	local target = Clockwork.player:FindByID(table.concat(arguments, " "));

	target:SetCharacterData("PDAUSER", nil);

	Clockwork.player:Notify(target, "Your username was reset by the Staff Team.")
	Clockwork.player:Notify(player, "You reset their username.")
end;

COMMAND:Register();