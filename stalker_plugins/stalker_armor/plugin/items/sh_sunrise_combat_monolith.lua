--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Monolith Combat Sunrise";
ITEM.model = "models/tnb/stalker/items/sunrise.mdl";
ITEM.uniqueID = "sunrise_combat_monolith";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 6;
ITEM.business = true;
ITEM.protection = 50;
ITEM.cost = 14000;
ITEM.description = "Offers little protection from gunfire or claws and it's absolutely useless against radiation or anomalies .";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local gender = player:GetGender();
		local model = "models/tnb/stalker/female_sunrise_combat.mdl";
		if (gender == GENDER_MALE) then
			model = "models/tnb/stalker/male_sunrise_combat.mdl";
		end;
		player:SetModel(model);
		player:SetSkin(3);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();