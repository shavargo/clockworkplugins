--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Freedom Exosuit";
ITEM.model = "models/tnb/stalker/items/exo.mdl";
ITEM.uniqueID = "exosuit_freedom";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 15;
ITEM.business = true;
ITEM.protection = 90;
ITEM.cost = 100000;
ITEM.description = "An experimental sample of a military exoskeleton. Was never mass-produced due to extraordinary cost and some design flaws. Despite this, it is in demand due to its ability to take on the weight of all carried equipment, and therefore small batches are made in underground facilities outside Ukraine. Comes with a built-in artifact container.";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local model = player:GetModel();
		local armor = "models/tnb/stalker/male_exo.mdl";
		player:SetModel(armor);
		player:SetSkin(2);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();