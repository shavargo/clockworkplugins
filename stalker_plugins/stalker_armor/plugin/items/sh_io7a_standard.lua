--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Standard IO7A";
ITEM.model = "models/tnb/stalker/items/io7a.mdl";
ITEM.uniqueID = "io7a";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 5;
ITEM.business = true;
ITEM.protection = 0;
ITEM.cost = 10000;
ITEM.description = "Its design is based on the suit used by the special forces of the Western armies. Due to a special treatment of the fabric, the armor has a strengthened stability during the physical movement of its plates.";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local model = player:GetModel();
		local armor = "models/tnb/stalker/male_io7a.mdl";
		player:SetModel(armor);
		player:SetSkin(0);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();