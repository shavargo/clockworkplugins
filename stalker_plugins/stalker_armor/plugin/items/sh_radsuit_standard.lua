--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Standard Radsuit";
ITEM.model = "models/tnb/stalker/items/radsuit.mdl";
ITEM.uniqueID = "radsuit";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 0;
ITEM.business = true;
ITEM.protection = 0;
ITEM.cost = 75000;
ITEM.description = "The suit is the pinnacle of personal protection technology. It is composed of three elements - a tactical helmet, a heavy armoured vest and reinforced jumpsuit. The tactical helmet encloses the entire head, covering the cranium with a thick armoured helmet, the face with a specially made gas mask with external filters and a cloth cowl covering the neck and ears.";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local model = player:GetModel();
		local armor = "models/tnb/stalker/male_radsuit.mdl";
		player:SetModel(armor);
		player:SetSkin(0);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();