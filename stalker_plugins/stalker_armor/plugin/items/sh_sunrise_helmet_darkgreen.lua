--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Dark Green Sunrise with Helmet";
ITEM.model = "models/tnb/stalker/items/sunrise.mdl";
ITEM.uniqueID = "sunrise_helmet_darkgreen";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 5;
ITEM.business = true;
ITEM.protection = 35;
ITEM.cost = 12000;
ITEM.description = "Offers little protection from gunfire or claws and it's absolutely useless against radiation or anomalies .";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local gender = player:GetGender();
		local model = "models/tnb/stalker/female_sunrise_helmet.mdl";
		if (gender == GENDER_MALE) then
			model = "models/tnb/stalker/male_sunrise_helmet.mdl";
		end;
		player:SetModel(model);
		player:SetSkin(11);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();