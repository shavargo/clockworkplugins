--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Brown Anorak";
ITEM.model = "models/tnb/stalker/items/anorak.mdl";
ITEM.uniqueID = "anorak_brown";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 0;
ITEM.business = true;
ITEM.protection = 0;
ITEM.cost = 500;
ITEM.description = "Offers little protection from gunfire or claws and it's absolutely useless against radiation or anomalies .";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		player:SetSkin(3);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:SetSkin(0);
	player:RemoveClothes();
end;

ITEM:Register();