--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Standard SEVA";
ITEM.model = "models/tnb/stalker/items/seva.mdl";
ITEM.uniqueID = "seva";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 9;
ITEM.business = true;
ITEM.protection = 50;
ITEM.cost = 40000;
ITEM.description = "This bodysuit, intended for conducting research in the Zone, combines a closed-cycle respiratory module and an external isolation coating, resulting in excellent protection from anomalies. Due to poor protection from physical impact, the suit is not a good defense against bullet and fragmentation damage. It comes with a built-in artifact transportation container.";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local model = player:GetModel();
		local armor = "models/tnb/stalker/male_exo.mdl";
		player:SetModel(armor);
		player:SetSkin(0);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();