--[[
	© 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New("clothes_base");

ITEM.name = "Freedom Berill";
ITEM.model = "models/tnb/stalker/items/berill.mdl";
ITEM.uniqueID = "berill_freedom";
ITEM.access = "0";
ITEM.batch = 1;
ITEM.weight = 7;
ITEM.business = true;
ITEM.protection = 40;
ITEM.cost = 22000;
ITEM.description = "Particularly durable suit and very resistant to firearm damage. Its only drawbacks are its below-average anomaly protection and weight.";

-- Called when a player changes clothes.
function ITEM:OnChangeClothes(player, bIsWearing)
	if (bIsWearing) then
		local model = player:GetModel();
		local armor = "models/tnb/stalker/male_berill.mdl";
		player:SetModel(armor);
		player:SetSkin(2);
	else
		Clockwork.player:SetDefaultModel(player);
		Clockwork.player:SetDefaultSkin(player);
	end;
	
	if (self.OnChangedClothes) then
		self:OnChangedClothes(player, bIsWearing);
	end;
end;

-- Called when a player has unequipped the item.
function ITEM:OnPlayerUnequipped(player, extraData)
	player:RemoveClothes();
end;

ITEM:Register();