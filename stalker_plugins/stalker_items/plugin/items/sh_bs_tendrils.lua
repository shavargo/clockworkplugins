--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Blood Sucker Tendrils";
ITEM.cost = 250;
ITEM.model = "models/jawkrovosos.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Food";
ITEM.description = "Medium sized tendrils from a mutant.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();