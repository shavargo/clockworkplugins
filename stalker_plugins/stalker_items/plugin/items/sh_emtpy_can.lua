--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Empty Can";
ITEM.cost = 25;
ITEM.model = "models/props_junk/garbage_metalcan002a.mdl";
ITEM.weight = 1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Junk";
ITEM.description = "An empty old can effected by the weather";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();