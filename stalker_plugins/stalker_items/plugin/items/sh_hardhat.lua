--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Ruined Hard Hat";
ITEM.cost = 120;
ITEM.model = "models/stalker/outfit/hardhat.mdl";
ITEM.weight = 1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Junk";
ITEM.description = "A plastic hard hat, once used for construction thats now broken..";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();