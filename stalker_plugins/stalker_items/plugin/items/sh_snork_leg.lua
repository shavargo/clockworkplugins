--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Snork Leg";
ITEM.cost = 75;
ITEM.model = "models/lagsnork.mdl";
ITEM.weight = 1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Food";
ITEM.description = "A extremely deformed leg of a mutant.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();