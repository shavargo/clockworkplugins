--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Boar Leg";
ITEM.cost = 125;
ITEM.model = "models/hgn/srp/items/boar-leg.mdl";
ITEM.weight = 1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Food";
ITEM.description = "The meaty hind leg of a Boar.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();