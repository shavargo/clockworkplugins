--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Pack of Shiborro";
ITEM.cost = 50;
ITEM.model = "models/hgn/srp/items/cigbox.mdl";
ITEM.weight = 0.1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Drugs";
ITEM.description = "A red pack of Shiborro 100s.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();