--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Empty Water Bottle";
ITEM.cost = 25;
ITEM.model = "models/stalker/item/medical/booster.mdl";
ITEM.weight = 1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Junk";
ITEM.description = "An empty clear plastic bottle.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();