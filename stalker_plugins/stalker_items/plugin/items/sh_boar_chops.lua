--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Boar Chops";
ITEM.cost = 100;
ITEM.model = "models/meat_boar.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.description = "Can make you full, but might poison you.";
ITEM.useText = "Eat";
ITEM.category = "Consumables";


-- Called when a player uses the item.
function ITEM:OnUse(player, itemEntity)
	player:SetHealth( math.Clamp( player:Health() + 10, 0, player:GetMaxHealth() ) );
	local sound = "hgn/stalker/player/pl_food.mp3";
	player:EmitSound(sound); 
end;

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();