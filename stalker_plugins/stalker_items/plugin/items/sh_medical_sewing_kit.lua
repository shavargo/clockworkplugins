--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Medical Sewing Kit";
ITEM.cost = 250;
ITEM.model = "models/sewing_kit_a.mdl";
ITEM.weight = 0.5;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Medical";
ITEM.description = "A kit that contains various tools for closing open wounds.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();