--[[
	� 2013 CloudSixteen.com do not share, re-distribute or modify
	without permission of its author (kurozael@gmail.com).
--]]

local ITEM = Clockwork.item:New();
ITEM.name = "Binoculars";
ITEM.cost = 50;
ITEM.model = "models/bin.mdl";
ITEM.weight = 0.1;
ITEM.access = "1";
ITEM.business = false;
ITEM.category = "Opticals";
ITEM.description = "Binoculars that appear to be extremely weathered but still capable of using.";

-- Called when a player drops the item.
function ITEM:OnDrop(player, position) end;

ITEM:Register();