local PLUGIN = PLUGIN;
local Clockwork = Clockwork;

-- Called when the menu's items should be destroyed.
function PLUGIN:MenuItemsDestroy(items)
	if (CLIENT) then
	    items:Destroy("Plugin Center");
	    items:Destroy("Community");
	    items:Destroy("Directory");
    end;
end;